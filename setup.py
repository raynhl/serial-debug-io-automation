###############################################################################

# File name: setup.py
# Author: Ray Han Lim Ng <ray.han.lim.ng@intel.com?
# Date created: 6/7/2017
# Date last modified: 6/7/2017
# Python Version: 2.7
# License: GNU GPL v2.0
# Description: 	This script functionality is to setup the required libraries and
#				requirements to use serial port utilities in this package. 
#
#				***Needs to be Run As Administrator.***

###############################################################################

import os
import time

def get_port_pairs(filename):
	file = open(filename, "r+")
	pairs = file.readlines()

	file.close()
	pairs = [pair.strip().split(':') for pair in pairs]
	return pairs


cwd = os.getcwd()
config_file_path = os.path.join(cwd, os.path.join("config", "pair_info.config"))
setup_files_path = os.path.join(cwd,"setup_files")
progress_bar_library_path = os.path.join(setup_files_path, "progressbar-2.3")
com0com_files_path = os.path.join(setup_files_path, os.path.join("com0com","x64"))


# Install the progressbar-library
os.chdir(progress_bar_library_path)
os.system("python setup.py install")

print("[SUCCESS] ProgressBarLibrary installed successfully!")


# Install com0com virtual ports drivers
PORT_PAIRS = get_port_pairs(config_file_path)
os.chdir(com0com_files_path)

for pair in PORT_PAIRS:
	os.system("setupc.exe --silent install PortName={} PortName={}".format(pair[0], pair[1]))
	time.sleep(2)

print("[SUCCESS] com0com virtual ports installed successfully!")

