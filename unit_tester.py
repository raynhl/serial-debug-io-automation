###############################################################################

# File name: unit_tester.py
# Author: Ray Han Lim Ng <ray.han.lim.ng@intel.com?
# Date created: 6/7/2017
# Date last modified: 6/7/2017
# Python Version: 2.7
# License: GNU GPL v2.0
# Description: 	This is a BIOS unit tester that is able to automate serial input from
#				a user provided test file. Particularly, it allows user to automate 
#				entry into the BIOS Setup Menu and change options automatically.

###############################################################################

import argparse
import time
import sys
import serial
import progressbar

KEYBOARD_F2_KEY = '\x1b2'

parser = argparse.ArgumentParser("Perform unit test.")
parser.add_argument('-v', help = "Virtual serial port", required=True)
parser.add_argument("-br", help = "Baud rate for serial port", type = int, default=115200)
parser.add_argument("-f", help = "Input file", required = True)
parser.add_argument("-t", help = "Delay between steps during automated input", type= int, default = 1)
parser.add_argument("-s", help = "Trigger string to enter BIOS Setup Screen", default = "Showing progress bar")
parser.add_argument("-p", help = "Postcode String for BIOS Setup Screen", default = "<AC>")

print("")

try:
	args = vars(parser.parse_args())

except:
	parser.print_help()
	sys.exit(0)

VIRTUAL_PORT = serial.Serial(args["v"], args["br"])
OUTPUT_FILE = open(args["f"], "rb")

print("[INFO] Scanning for trigger string: \"{}\"".format(args["s"]))
print("")

trigger_bar = progressbar.ProgressBar(maxval= 50, \
    widgets=[progressbar.Bar('>', '[', ']'), " Scanning for trigger string", progressbar.Percentage(), ' ', progressbar.ETA()])
trigger_bar.start()
bar_counter = 1


while args["s"] not in VIRTUAL_PORT.readline():
	trigger_bar.update(bar_counter)
	bar_counter += 1
	bar_counter %= 50

trigger_bar.finish()


print("")
print("")

print("[FOUND] Trigger string: \"{}\"".format(args["s"]))
time.sleep(0.1)
print("[INFO] Attempting to enter BIOS Setup Screen")
print("")

emulation_bar = progressbar.ProgressBar(maxval= 50, \
    widgets=[progressbar.Bar('*', '[', ']'), " Emulating F2 Key Press", progressbar.Percentage(), ' ', progressbar.ETA()])
emulation_bar.start()
bar_counter = 1

while args["p"] not in VIRTUAL_PORT.readline():
	VIRTUAL_PORT.write(KEYBOARD_F2_KEY)
	emulation_bar.update(bar_counter)
	bar_counter += 1
	bar_counter %= 50
	time.sleep(0.10)

emulation_bar.finish()
print("")
print("[SUCCESS] Entered BIOS Setup Menu")

time.sleep(10)
print("[INFO] Starting automated input.")
time.sleep(0.5)
print("[INFO] Reading automated input test file")
time.sleep(0.5)

input_sequences = OUTPUT_FILE.readlines()
OUTPUT_FILE.close()
input_sequences = [step.rstrip('/n') for step in input_sequences]

time.sleep(0.5)
print("[INFO] Test file read successful.")
time.sleep(0.5)
print("[INFO] Performing automated input")
print("")
time.sleep(0.5)

# Setup progress bar to show progress of automated stepping
steps_remaining = len(input_sequences)
automation_bar = progressbar.ProgressBar(maxval= steps_remaining, \
    widgets=[progressbar.Bar('=', '[', ']'), " Test ", progressbar.Percentage(), ' ', progressbar.ETA()])
automation_bar.start()
bar_counter = 1

for step in input_sequences:
	automation_bar.update(bar_counter)
	VIRTUAL_PORT.write(bytearray(step))
	bar_counter += 1
	time.sleep(args["t"])

print("[")
print("")
print("[DONE] Automated input completed.")
time.sleep(0.5)
print("[INFO] Exiting script now.")