###############################################################################

# File name: serial_port_splitter.py
# Author: Ray Han Lim Ng <ray.han.lim.ng@intel.com?
# Date created: 6/7/2017
# Date last modified: 6/7/2017
# Python Version: 2.7
# License: GNU GPL v2.0
# Description: 	This is a virtual serial port splitter that can split
#				one hardware serial port into multiple virtual serial port.
#				
#				It also provides functionality to record raw serial input for
#				playback during automated serial input.

###############################################################################


import argparse
import threading
import time
import sys
import serial
import os
import subprocess

def port_read_listener(physical_port, virtual_port):
		
		port_name = find_port_pair(virtual_port.port)

		if port_name is not None:
			print("[INFO] Virtual Serial Port started: {}".format(port_name))
			time.sleep(0.01)
			
			while True:
				try:
					if virtual_port.in_waiting:
						reading = virtual_port.read(virtual_port.in_waiting)
						physical_port.write(reading.encode())

						if args["r"]:
							OUTPUT_FILE.write(reading + '\n')

						time.sleep(0.05)


				except(KeyboardInterrupt,SystemExit):
					print("")
					print("[ATTENTION] Interrupt detected! Stopping and exitting program.")
					sys.exit(0)

def find_port_pair(port_name):

	global PORT_PAIRS

	for pair in PORT_PAIRS:
		if port_name == pair[0]:
			return pair[1]

		elif port_name == pair[1]:
			return pair[0]

	return None

def get_port_pairs(filename):
	file = open(filename, "r+")
	pairs = file.readlines()

	file.close()
	pairs = [pair.strip().split(':') for pair in pairs]
	return pairs

def send_message(port, message):
	port.write(message)
	time.sleep(0.001)

				

parser = argparse.ArgumentParser("Split a physical serial port into multiple virtual serial port(s).")
parser.add_argument('-b', help = "Physical serial port", required=True)
parser.add_argument('-n', help = "Number of virtual serial port", type= int, required=True)
parser.add_argument('-v', help = "Virtual serial port(s) [eg: COM1,COM2,COM3]", required = True)
parser.add_argument("-br", help = "Baud rate for all serial ports", type = int, default=115200)
parser.add_argument("-r", help = "Record keystrokes to <filename>", default = None)
print("")

try:
	args = vars(parser.parse_args())

except:
	parser.print_help()
	sys.exit(0)

virtual_port_names = [arg.strip() for arg in args["v"].split(',')]

if len(virtual_port_names) is not args["n"]:
	print("")
	print("[ERROR] Virtual port(s) definition does not match number of virtual ports.")
	print("")
	parser.print_help()
	sys.exit(0)

if args["r"]:
	try:
		OUTPUT_FILE = open(args["r"], "wb")

	except:
		parser.print_help()
		sys.exit(0)

BAUD_RATE = args["br"]

PHYSICAL_PORT = serial.Serial(args["b"], BAUD_RATE)
VIRTUAL_PORTS = []
PORT_PAIRS = get_port_pairs("config/pair_info.config")

for virtual_port_name in virtual_port_names:
	VIRTUAL_PORTS.append(serial.Serial(virtual_port_name, BAUD_RATE, writeTimeout=0))

for virtual_port in VIRTUAL_PORTS:
	thread = threading.Thread(target=port_read_listener, args= (PHYSICAL_PORT,virtual_port))
	thread.daemon = True
	thread.start()
	time.sleep(0.01)

try:
	while True:
		line = PHYSICAL_PORT.readline()
		line_stripped = line.rstrip('\n')

		for virtual_port in VIRTUAL_PORTS:
			message_thread = threading.Thread(target = send_message, args = (virtual_port, line))
			message_thread.daemon = True
			message_thread.start()

except(KeyboardInterrupt,SystemExit):
	print("")
	print("[ATTENTION] Interrupt detected! Stopping and exitting program.")
	OUTPUT_FILE.close()
	sys.exit(0)